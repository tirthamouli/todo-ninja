import firebase from "firebase/app";
import "firebase/firestore";

// Initialize firebase
var config = {
  apiKey: "AIzaSyAysC5oX8M_kOPlf4d6jYGJPZWI-GUL0gE",
  authDomain: "todo-ninja-214fd.firebaseapp.com",
  databaseURL: "https://todo-ninja-214fd.firebaseio.com",
  projectId: "todo-ninja-214fd",
  storageBucket: "todo-ninja-214fd.appspot.com",
  messagingSenderId: "240829237463"
};
firebase.initializeApp(config);

const db = firebase.firestore();
export default db;
